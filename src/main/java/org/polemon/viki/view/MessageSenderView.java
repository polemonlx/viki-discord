package org.polemon.viki.view;

import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.user.User;

public interface MessageSenderView {

    void sendToChannel(TextChannel textChannel, String message);


    void sendToUser(User user, String message);

}
