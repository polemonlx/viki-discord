package org.polemon.viki.view;


public interface MessageListenerView {

    void startListening();

}
