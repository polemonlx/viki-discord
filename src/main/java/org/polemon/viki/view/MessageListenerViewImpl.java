package org.polemon.viki.view;

import org.javacord.api.DiscordApi;
import org.javacord.api.event.message.MessageCreateEvent;
import org.polemon.viki.controller.message.MessageListenerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageListenerViewImpl implements MessageListenerView {

    private DiscordApi api;

    private MessageListenerController messageListenerController;


    @Autowired
    public void setApi(DiscordApi api) {
        this.api = api;
    }


    @Autowired
    public void setMessageListenerController(MessageListenerController messageListenerController) {
        this.messageListenerController = messageListenerController;
    }


    @Override
    public void startListening() {
        api.addMessageCreateListener(this::messageReader);
    }


    private void messageReader(MessageCreateEvent event) {
        /*
        if (event.getMessageAuthor().isBotUser()) {
            return;
        }
         */

        String message = event.getMessageContent().toLowerCase();

        if (!message.startsWith("viki")) {
            return;
        }

        messageListenerController.execute(event, "");
    }

}
