package org.polemon.viki.view;

import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.user.User;
import org.springframework.stereotype.Component;

import java.awt.*;

@Component
public class MessageSenderViewImpl implements MessageSenderView {

    @Override
    public void sendToChannel(TextChannel textChannel, String message) {
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setDescription(message);
        embedBuilder.setColor(Color.GREEN);
        textChannel.sendMessage(embedBuilder);
    }


    @Override
    public void sendToUser(User user, String message) {
        user.sendMessage(message);
    }

}
