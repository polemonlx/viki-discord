package org.polemon.viki;

import org.javacord.api.DiscordApi;
import org.polemon.viki.controller.activity.ActivityDetection;
import org.polemon.viki.controller.message.command.*;
import org.polemon.viki.view.MessageListenerView;
import org.springframework.context.support.GenericXmlApplicationContext;

public class Viki {

    public static void main(String[] args) {

        Viki viki = new Viki();

        MessageListenerView messageListenerView = viki.bootstrap();
        messageListenerView.startListening();

        OnBoot onBoot = new OnBoot();
        onBoot.init();
    }


    private MessageListenerView bootstrap() {

        // Spring create beans
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.getEnvironment().setActiveProfiles(Config.ACTIVE_PROFILE);
        ctx.load(Config.RESOURCES_LOCATION);
        ctx.refresh();


        // Set beans to Api
        DiscordApi api = ctx.getBean(DiscordApi.class);

        api.addServerVoiceChannelMemberJoinListener(ctx.getBean(ActivityDetection.class));
        api.addServerVoiceChannelMemberLeaveListener(ctx.getBean(ActivityDetection.class));


        // Set beans to CommandEnum
        CommandEnum.HELP.setCommand(ctx.getBean(HelpCommand.class));
        CommandEnum.PING.setCommand(ctx.getBean(PingCommand.class));
        CommandEnum.REGISTER.setCommand(ctx.getBean(RegisterCommand.class));
        CommandEnum.CHALLENGE.setCommand(ctx.getBean(ChallengeCommand.class));
        CommandEnum.SETTINGS.setCommand(ctx.getBean(SettingsCommand.class));
        CommandEnum.TEST.setCommand(ctx.getBean(TestCommand.class));
        CommandEnum.INVALID.setCommand(ctx.getBean(InvalidCommand.class));


        // Setup Threads
        Thread activityDetectionThread = new Thread(ctx.getBean(ActivityDetection.class));
        activityDetectionThread.start();

        return ctx.getBean(MessageListenerView.class);
    }

}
