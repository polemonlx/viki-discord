package org.polemon.viki;

public class Messages {

    public static final String HELLO_COMMAND = "How are you today?\n" +
            "Made with love by Polemon (he is my boss, shh)\n" +
            "I am currently on version 2.0.0";
    public static final String PONG_COMMAND = "pong (I love to play this game =) )";
    public static final String HELP_COMMAND = "VIKI at your service!\n" +
            "Every command needs to start with \"viki\". It is the way I have to know that you are talking to me =)\n" +
            "Only writing \"viki\" will print a welcome message and my version.\n" +
            "Available commands:\n\n";
    public static final String INVALID_COMMAND = "Wrong command input. Use: viki help";

    public static final String SUCCESS_REGISTER = "You successful registered! You can now challenge your friends in many games.";
    public static final String SUCCESS_REMOVED = "You have been successfully removed.";

    public static final String ERROR_PLAYER_NOT_FOUND = "The provided player was not found in this text channel.";
    public static final String ERROR_CALL_OWN_PLAYER = "You cannot challenge yourself.";
    public static final String ERROR_CALL_REPEATED_PLAYER = "You cannot challenge repeated players.";
    public static final String ERROR_GAME_NAME_NOT_FOUND = "The provided game doesn't exist.";
    public static final String ERROR_NOT_ENOUGH_PLAYERS = "Not enough players to play.";
    public static final String ERROR_TO_MUCH_PLAYERS = "To much players invited.";
    public static final String ERROR_PLAYER1_ALREADY_IN_GAME = "You are already in a multiplayer game.";
    public static final String ERROR_PLAYER2_ALREADY_IN_GAME = "The provided player is already playing another multiplayer game.";
    public static final String ERROR_MULTI_APPLICATIONS = "You have more than 1 application at the same time. Use: viki <application> <arguments>";
    public static final String ERROR_NO_NUMBER_FOUND = "You didn't input a number.";
    public static final String ERROR_NOT_ENOUGH_MONEY = "You don't have enough money.";
    public static final String ERROR_INVALID_BET = "You cannot bet a number below 0.";
    public static final String ERROR_ALREADY_REGISTERED = "You are already registered bro.";
    public static final String ERROR_UNEXPECTED = "An unexpected error occurred. Please, contact Polemon and report the problem.";

}