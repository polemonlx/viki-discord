package org.polemon.viki.game.util;

import org.javacord.api.entity.user.User;

import java.util.LinkedHashMap;
import java.util.Map;

public class Classification {

    private Map<Long, Integer> playerRewardMap = new LinkedHashMap<>();

    private int bet;


    public Map<Long, Integer> getPlayerRewardMap() {
        return playerRewardMap;
    }


    public void setPlayerRewardMap(Map<Long, Integer> playerRewardMap) {
        this.playerRewardMap = playerRewardMap;
    }


    public int getBet() {
        return bet;
    }


    public void setBet(int bet) {
        this.bet = bet;
    }


    public void addPlayerReward(User user, int reward) {
        playerRewardMap.put(user.getId(), reward);
    }

}
