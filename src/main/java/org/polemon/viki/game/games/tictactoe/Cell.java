package org.polemon.viki.game.games.tictactoe;

public class Cell {

    private final int column;
    private final int row;

    private String figure;


    public Cell(int column, int row) {
        this.column = column;
        this.row = row;
    }


    public String getFigure() {
        if (figure == null) {
            return "   ";
        }
        return figure;
    }


    public void setFigure(String figure) {
        this.figure = figure;
    }

}
