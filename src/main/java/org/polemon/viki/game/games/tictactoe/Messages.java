package org.polemon.viki.game.games.tictactoe;

public class Messages {

    public static final String VICTORY = "VICTORY!";

    public static final String ERROR_NOT_YOUR_TURN = "Not your turn.";
    public static final String ERROR_NOT_VALID_POSITION = "Not a valid place to play. Try again.";
    public static final String ERROR_PLACE_ALREADY_TAKEN = "The place you chose it's already taken.";

}
