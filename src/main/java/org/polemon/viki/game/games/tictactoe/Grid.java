package org.polemon.viki.game.games.tictactoe;

import org.polemon.viki.game.controller.TicTacToeController;

public class Grid {

    private final int numberColumns;
    private final int numberRows;

    private Cell[][] cells;


    public Grid(int numberColumns, int numberRows) {
        this.numberColumns = numberColumns;
        this.numberRows = numberRows;

        cells = new Cell[numberColumns][numberRows];

        for (int col = 0; col < numberColumns; col++) {
            for (int row = 0; row < numberRows; row++) {
                cells[col][row] = new Cell(col, row);
            }
        }
    }


    @Override
    public String toString() {

        StringBuilder grid = new StringBuilder();

        grid.append("-------------\n");

        for (int row = 0; row < numberRows; row++) {
            for (int col = 0; col < numberColumns; col++) {
                grid.append("|  ").append(cells[col][row].getFigure()).append("  ");
            }
            grid.append("|\n-------------\n");
        }

        return grid.toString();
    }


    public boolean addFigure(int col, int row, String figure) {
        if (cells[col][row].getFigure() != "   ") {
            return false;
        }
        cells[col][row].setFigure(figure);
        return true;
    }


    public boolean getRowVictory(int row) {
        int sum = 0;
        for (int col = 0; col < numberColumns; col++) {
            if (cells[col][row].getFigure().equals(TicTacToeController.CROSS)) {
                sum += 1;
            } else if (cells[col][row].getFigure().equals(TicTacToeController.CIRCLE)) {
                sum -= 1;
            }
        }
        return Math.abs(sum) == numberColumns;
    }


    public boolean getColVictory(int col) {
        int sum = 0;
        for (int row = 0; row < numberRows; row++) {
            if (cells[col][row].getFigure().equals(TicTacToeController.CROSS)) {
                sum += 1;
            } else if (cells[col][row].getFigure().equals(TicTacToeController.CIRCLE)) {
                sum -= 1;
            }
        }
        return Math.abs(sum) == numberColumns;
    }


    public boolean getDiagonalVictory(int col, int row) {

        int sum = 0;
        if (col == row) {
            for (int i = 0; i < numberColumns; i++) {
                if (cells[i][i].getFigure().equals(TicTacToeController.CROSS)) {
                    sum += 1;
                } else if (cells[i][i].getFigure().equals(TicTacToeController.CIRCLE)) {
                    sum -= 1;
                }
            }
        }

        if (Math.abs(sum) == 3) {
            return true;
        }

        sum = 0;
        if (col + row == numberColumns - 1) {
            for (int i = numberColumns - 1; i >= 0; i--) {
                if (cells[i][numberColumns - 1 - i].getFigure().equals(TicTacToeController.CROSS)) {
                    sum += 1;
                } else if (cells[i][i].getFigure().equals(TicTacToeController.CIRCLE)) {
                    sum -= 1;
                }
            }
        }
        return Math.abs(sum) == 3;
    }

}
