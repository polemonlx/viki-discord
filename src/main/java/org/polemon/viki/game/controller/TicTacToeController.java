package org.polemon.viki.game.controller;

import org.javacord.api.entity.user.User;
import org.polemon.viki.game.games.tictactoe.Grid;
import org.polemon.viki.game.games.tictactoe.Messages;
import org.polemon.viki.listener.game.EndGame;

import java.util.*;

public class TicTacToeController implements GameController {

    public static final String CROSS = "X";
    public static final String CIRCLE = "0";

    private final Map<User, String> playersMap = new LinkedHashMap<>();

    private final List<User> playersEliminated = new LinkedList<>();

    private final List<User> playersQuited = new LinkedList<>();

    private EndGame endGame;

    private User playerTurn;

    private Grid grid;

    private int numberColumns = 3;
    private int numberRows = 3;


    @Override
    public String init(List<User> players, EndGame endGame, String[] options) {

        this.endGame = endGame;

        getPlayersMap(players);

        int number = (int) (Math.random() * 2);
        playerTurn = (User) playersMap.keySet().toArray()[number];

        if (options != null) {
            getOptions(options);
        }
        grid = new Grid(numberColumns, numberRows);

        return playerTurn.getMentionTag() + " goes first. You play with " + playersMap.get(playerTurn) + " figure.\n" + grid.toString();
    }


    @Override
    public String execute(User user, String[] gameArguments) {

        if (!isUserTurn(user)) {
            return Messages.ERROR_NOT_YOUR_TURN;
        }

        int placeToPlay;
        try {
            placeToPlay = getCellPlayed(gameArguments);
        } catch (NumberFormatException ex) {
            return Messages.ERROR_NOT_VALID_POSITION;
        }

        if (updateBoard(placeToPlay)) {
            if (hasWon(placeToPlay)) {
                finish();
                return Messages.VICTORY + "\nThe player " + playerTurn.getMentionTag() + " has won the game.\n" + grid.toString();
            }
            changePlayer();
            return playerTurn.getMentionTag() + " it's your turn.\n" + grid.toString();
        }

        return Messages.ERROR_PLACE_ALREADY_TAKEN;
    }


    @Override
    public void userQuit(User player) {
        playersQuited.add(player);
    }


    private boolean hasWon(int placeToPlay) {

        int row = (int) Math.ceil(placeToPlay / 3d);
        int col = placeToPlay - ((row - 1) * 3);
        row -= 1;
        col -= 1;

        return grid.getRowVictory(row) || grid.getColVictory(col) || grid.getDiagonalVictory(col, row);
    }


    private void changePlayer() {
        Set<User> users = playersMap.keySet();
        if (users.toArray()[0].equals(playerTurn)) {
            playerTurn = (User) users.toArray()[1];
            return;
        }
        playerTurn = (User) users.toArray()[0];
    }


    private boolean updateBoard(int placeToPlay) {
        int row = (int) Math.ceil(placeToPlay / 3d);
        int col = placeToPlay - ((row - 1) * 3);
        row -= 1;
        col -= 1;

        return grid.addFigure(col, row, playersMap.get(playerTurn));
    }


    private void finish() {

        List<User> players = new LinkedList<>();
        players.add(playerTurn);
        playersMap.remove(playerTurn);
        players.add(playersMap.keySet().stream().findFirst().get());

        endGame.finish(players);
    }


    private void getPlayersMap(List<User> players) {

        int number = (int) (Math.random() * 2);

        if (number == 0) {
            playersMap.put(players.get(1), CIRCLE);
            playersMap.put(players.get(0), CROSS);
        } else {
            playersMap.put(players.get(1), CROSS);
            playersMap.put(players.get(0), CIRCLE);
        }
    }


    private void getOptions(String[] options) {
        try {
            numberColumns = Integer.parseInt(options[0]);
            numberRows = Integer.parseInt(options[1]);
        } catch (NumberFormatException ex) {
            numberColumns = 3;
            numberRows = 3;
        }
    }


    private boolean isUserTurn(User user) {
        return playerTurn.equals(user);
    }


    private int getCellPlayed(String[] gameArguments) throws NumberFormatException {
        int position = Integer.parseInt(gameArguments[0]);
        if (position < 1 || position > 9) {
            throw new NumberFormatException();
        }
        return position;
    }

}
