package org.polemon.viki.game.controller;

import org.javacord.api.entity.user.User;
import org.polemon.viki.listener.game.EndGame;

import java.util.List;

public interface GameController {

    String init(List<User> players, EndGame endGame, String[] options);

    String execute(User user, String[] gameArguments);

    void userQuit(User user);

}
