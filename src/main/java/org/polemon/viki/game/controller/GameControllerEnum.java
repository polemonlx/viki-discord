package org.polemon.viki.game.controller;

import java.lang.reflect.InvocationTargetException;

public enum GameControllerEnum {
    TICTACTOE(TicTacToeController.class, 2, 2),

    INVALID(null, 0, 0);

    private final Class<? extends GameController> gameControllerClass;

    private final int minPlayers;
    private final int maxPlayers;


    GameControllerEnum(Class<? extends GameController> gameControllerClass, int minPlayers, int maxPlayers) {
        this.gameControllerClass = gameControllerClass;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
    }


    public GameController getGameController() {

        if (gameControllerClass == null) {
            return null;
        }

        try {
            return gameControllerClass.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }


    public Class<? extends GameController> getGameControllerClass() {
        return gameControllerClass;
    }


    public int getMinPlayers() {
        return minPlayers;
    }


    public int getMaxPlayers() {
        return maxPlayers;
    }

}
