package org.polemon.viki.controller.activity;

import org.javacord.api.DiscordApi;
import org.javacord.api.event.channel.server.voice.ServerVoiceChannelMemberJoinEvent;
import org.javacord.api.event.channel.server.voice.ServerVoiceChannelMemberLeaveEvent;
import org.javacord.api.listener.channel.server.voice.ServerVoiceChannelMemberJoinListener;
import org.javacord.api.listener.channel.server.voice.ServerVoiceChannelMemberLeaveListener;
import org.polemon.viki.Config;
import org.polemon.viki.persistence.model.User;
import org.polemon.viki.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ActivityDetection implements ServerVoiceChannelMemberJoinListener, ServerVoiceChannelMemberLeaveListener, Runnable {

    private DiscordApi api;

    private UserService userService;


    @Autowired
    public void setApi(DiscordApi api) {
        this.api = api;
    }


    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    @Override
    public void onServerVoiceChannelMemberJoin(ServerVoiceChannelMemberJoinEvent event) {
        User user = userService.get(event.getUser().getId());
        if (user != null)
            userService.activityReward(event.getUser().getId());
    }


    @Override
    public void onServerVoiceChannelMemberLeave(ServerVoiceChannelMemberLeaveEvent event) {
        User user = userService.get(event.getUser().getId());
        if (user != null)
            userService.activityReward(event.getUser().getId());
    }


    @Override
    public void run() {

        try {

            while (true) {

                for (User user : userService.onlineUsers()) {
                    userService.activityReward(user.getId());
                }

                Thread.sleep(Config.UPDATE_STATUS_INTERVAL_MILLISECONDS);
            }

        } catch (Exception ex) {
            ex.getStackTrace();
            run();
        }

    }

}
