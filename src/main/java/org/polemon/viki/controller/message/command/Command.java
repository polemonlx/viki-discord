package org.polemon.viki.controller.message.command;

import org.javacord.api.event.message.MessageCreateEvent;

public interface Command {

    String execute(MessageCreateEvent event);

}
