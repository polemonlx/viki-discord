package org.polemon.viki.controller.message;

import org.javacord.api.event.message.MessageCreateEvent;

public interface MessageListenerController {

    void execute(MessageCreateEvent event, String message);

}
