package org.polemon.viki.controller.message;

import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.polemon.viki.Messages;
import org.polemon.viki.controller.message.command.Command;
import org.polemon.viki.controller.message.command.CommandEnum;
import org.polemon.viki.listener.ActivityListener;
import org.polemon.viki.listener.ListenerManager;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Controller
public class MessageListenerControllerImpl implements MessageListenerController {

    private MessageSenderController messageSenderController;

    private ListenerManager listenerManager;


    @Autowired
    public void setMessageSenderController(MessageSenderController messageSenderController) {
        this.messageSenderController = messageSenderController;
    }


    @Autowired
    public void setUserManager(ListenerManager listenerManager) {
        this.listenerManager = listenerManager;
    }


    @Override
    public void execute(MessageCreateEvent event, String message) {

        // Transform message in lower case
        String incomingMessage = event.getMessageContent().toLowerCase();

        // If message is only "viki"
        if (incomingMessage.equals("viki")) {
            messageSenderController.execute(event, "Hi " + "<@!" + event.getMessageAuthor().getIdAsString() + ">. " + Messages.HELLO_COMMAND);
            return;
        }

        // Else, execute commands
        String response = executeCommand(event, incomingMessage);

        // Ask sender to send response
        if (response != null) {
            messageSenderController.execute(event, response);
        }

    }


    private String executeCommand(MessageCreateEvent event, String incomingMessage) {

        String[] arguments = incomingMessage.split(" ", 3);

        Command command;
        try {
            command = CommandEnum.valueOf(arguments[1].toUpperCase()).getCommand();
        } catch (IllegalArgumentException ex) {

            User user = event.getMessageAuthor().asUser().get();
            if (listenerManager.countListeners(user) > 0) {
                String listenerResponse = tryGetListener(user, arguments);

                if (listenerResponse != null) {
                    return listenerResponse;
                }
            }

            command = CommandEnum.INVALID.getCommand();
        }

        return command.execute(event);
    }


    private String tryGetListener(User user, String[] arguments) {

        if (arguments.length < 3) {
            if (listenerManager.countListeners(user) > 1) {
                return Messages.ERROR_MULTI_APPLICATIONS;
            }
        }

        ActivityListener listener = listenerManager.getListener(user, arguments);

        return listener == null ? null : listener.execute(user, arguments);
    }

}
