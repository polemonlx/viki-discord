package org.polemon.viki.controller.message;

import org.javacord.api.event.message.MessageCreateEvent;
import org.polemon.viki.view.MessageSenderView;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Controller
public class MessageSenderControllerImpl implements MessageSenderController {

    private MessageSenderView messageSenderView;


    @Autowired
    public void setMessageSender(MessageSenderView messageSenderView) {
        this.messageSenderView = messageSenderView;
    }


    @Override
    public void execute(MessageCreateEvent event, String message) {
        messageSenderView.sendToChannel(event.getChannel(), message);
    }

}


/*
    User user = event.getApi().getCachedUserById(event.getMessageAuthor().getId()).get();
    TextChannel textChannel = user.openPrivateChannel().join();
    messageSenderView.sendToChannel(textChannel, "s");
*/