package org.polemon.viki.controller.message.command;

import org.javacord.api.event.message.MessageCreateEvent;
import org.polemon.viki.Messages;
import org.springframework.stereotype.Component;

@Component
public class HelpCommand implements Command {

    @Override
    public String execute(MessageCreateEvent event) {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(Messages.HELP_COMMAND);

        for (CommandEnum commandEnum : CommandEnum.values()) {
            stringBuilder.append(commandEnum.getHelpMessage());
        }

        return stringBuilder.toString();
    }

}
