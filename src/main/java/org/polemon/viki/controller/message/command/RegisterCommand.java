package org.polemon.viki.controller.message.command;

import org.javacord.api.event.message.MessageCreateEvent;
import org.polemon.viki.persistence.model.User;
import org.polemon.viki.service.UserService;
import org.polemon.viki.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RegisterCommand implements Command {

    private UserService userService;


    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    @Override
    public String execute(MessageCreateEvent event) {

        long id = event.getMessageAuthor().getId();
        String username = event.getMessageAuthor().getName();

        if (userService.get(id) != null) {
            return Messages.ERROR_ALREADY_REGISTERED;
        }

        if (insertUser(id, username)) {
            return Messages.SUCCESS_REGISTER;
        }

        return Messages.ERROR_UNEXPECTED;
    }


    private boolean insertUser(long id, String username) {

        User user = new User(id, username);
        return userService.save(user) != null;
    }

}
