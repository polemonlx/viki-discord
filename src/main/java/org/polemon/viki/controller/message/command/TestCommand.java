package org.polemon.viki.controller.message.command;

import org.javacord.api.event.message.MessageCreateEvent;
import org.polemon.viki.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestCommand implements Command {

    private UserService userService;


    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    @Override
    public String execute(MessageCreateEvent event) {
        return "Testing...";
    }

}
