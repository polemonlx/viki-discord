package org.polemon.viki.controller.message.command;

import org.javacord.api.entity.user.User;
import org.javacord.api.entity.user.UserStatus;
import org.javacord.api.event.message.MessageCreateEvent;
import org.polemon.viki.Messages;
import org.polemon.viki.dto.ChallengeGameDto;
import org.polemon.viki.game.controller.GameController;
import org.polemon.viki.game.controller.GameControllerEnum;
import org.polemon.viki.listener.ListenerManager;
import org.polemon.viki.listener.game.Bet;
import org.polemon.viki.listener.game.GameListenerMultiplayer;
import org.polemon.viki.listener.game.PlayingMultiplayer;
import org.polemon.viki.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class ChallengeCommand implements Command {

    private ListenerManager listenerManager;

    private UserService userService;


    @Autowired
    public void setUserManager(ListenerManager listenerManager) {
        this.listenerManager = listenerManager;
    }


    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    @Override
    public String execute(MessageCreateEvent event) {

        String message = event.getMessageContent();
        String[] arguments = message.split(" ");

        if (arguments.length < 4) {
            return "Usage: " + CommandEnum.CHALLENGE.getHelpMessage();
        }

        List<User> players = getUsersList(event, arguments);

        GameController game = getGame(arguments[arguments.length - 1]);

        int minPlayers = getMinPlayers(game);
        int maxPlayers = getMaxPlayers(game);

        String errors = canPlay(players, game, minPlayers, maxPlayers);
        if (!errors.isEmpty()) {
            return errors;
        }

        ChallengeGameDto challengeGameDto = createChallengeGameDto(players, game, arguments[3], minPlayers, maxPlayers);

        return addListener(challengeGameDto, players, arguments[3]);
    }


    private String addListener(ChallengeGameDto challengeGameDto, List<User> players, String arguments) {

        PlayingMultiplayer nextListener = new Bet(challengeGameDto);
        String response = "<@!" + players.get(0).getIdAsString() + "> has invited " + "<@!" + players.get(1).getIdAsString() + "> to play " + arguments + " game.\n" +
                "Place your bets";

        if (!isAllRegistered(players)) {
            nextListener = new GameListenerMultiplayer(challengeGameDto);
            response = ((GameListenerMultiplayer) nextListener).init();
        }

        for (User user : players) {
            listenerManager.addListener(user, nextListener);
        }

        return response;
    }


    private boolean isAllRegistered(List<User> players) {
        for (User user : players) {
            if (userService.get(user.getId()) == null) {
                return false;
            }
        }
        return true;
    }


    private int getMaxPlayers(GameController game) {
        if (game == null) {
            return 0;
        }

        for (GameControllerEnum gameControllerEnum : GameControllerEnum.values()) {
            if (gameControllerEnum.getGameControllerClass() == game.getClass()) {
                return gameControllerEnum.getMaxPlayers();
            }
        }
        return 0;
    }


    private int getMinPlayers(GameController game) {

        if (game == null) {
            return 0;
        }

        for (GameControllerEnum gameControllerEnum : GameControllerEnum.values()) {
            if (gameControllerEnum.getGameControllerClass() == game.getClass()) {
                return gameControllerEnum.getMinPlayers();
            }
        }
        return 0;
    }


    private List<User> getUsersList(MessageCreateEvent event, String[] arguments) { // viki challenge polemon, meh, ele tictactoe

        List<User> players = new LinkedList<>();

        players.add(event.getMessageAuthor().asUser().get());
        players.add(getUserByName(event, arguments));

        return players;
    }


    private User getUserByName(MessageCreateEvent event, String[] arguments) {

        String opponentName = "";
        for (int index = 2; index < arguments.length - 1; index++) {
            opponentName += arguments[index] + " ";
        }
        opponentName = opponentName.trim();

        for (User user : event.getApi().getCachedUsers()) {
            if (user.getName().equalsIgnoreCase(opponentName) || user.getDisplayName(event.getServer().get()).equalsIgnoreCase(opponentName)) {
                if (user.getStatus() != UserStatus.OFFLINE && user.getStatus() != UserStatus.INVISIBLE) {
                    if (event.getServerTextChannel().get().canWrite(user)) {
                        return user;
                    }
                }
            }
        }

        return null;
    }


    private String canPlay(List<User> players, GameController game, int minPlayers, int maxPlayers) {

        for (int i = 0; i < players.size() - 1; i++) {
            for (int j = i + 1; j < players.size(); j++) {
                if (players.get(i).equals(players.get(j))) {
                    if (players.get(j).equals(players.get(0))) {
                        return Messages.ERROR_CALL_OWN_PLAYER;
                    }
                    return Messages.ERROR_CALL_REPEATED_PLAYER;
                }
            }
        }

        for (User player : players) {
            if (player == null) {
                return Messages.ERROR_PLAYER_NOT_FOUND;
            }
            if (listenerManager.isPlayingMultiplayer(player)) {
                return Messages.ERROR_PLAYER2_ALREADY_IN_GAME;
            }
        }

        if (listenerManager.isPlayingMultiplayer(players.get(0))) {
            return Messages.ERROR_PLAYER1_ALREADY_IN_GAME;
        }

        if (game == GameControllerEnum.INVALID.getGameController() || game == null) {
            return Messages.ERROR_GAME_NAME_NOT_FOUND;
        }

        if (players.size() < minPlayers) {
            return Messages.ERROR_NOT_ENOUGH_PLAYERS + " Min: " + minPlayers;
        }

        if (players.size() > maxPlayers) {
            return Messages.ERROR_TO_MUCH_PLAYERS + " Max: " + maxPlayers;
        }

        return "";
    }


    private GameController getGame(String game) {

        GameController gameController;

        if (game.equalsIgnoreCase("random")) {
            int random = (int) (Math.random() * (GameControllerEnum.values().length - 1));
            gameController = GameControllerEnum.values()[random].getGameController();
            return gameController;
        }

        try {
            gameController = GameControllerEnum.valueOf(game.toUpperCase()).getGameController();
        } catch (IllegalArgumentException ex) {
            gameController = GameControllerEnum.INVALID.getGameController();
        }

        return gameController;
    }


    private ChallengeGameDto createChallengeGameDto(List<User> players, GameController gameController, String applicationName, int minPlayers, int maxPlayers) {

        ChallengeGameDto challengeGameDto = new ChallengeGameDto();

        challengeGameDto.setName(applicationName);
        challengeGameDto.setPlayers(players);
        challengeGameDto.setMinPlayers(minPlayers);
        challengeGameDto.setMaxPlayers(maxPlayers);
        challengeGameDto.setGameController(gameController);
        challengeGameDto.setListenerManager(listenerManager);
        challengeGameDto.setUserService(userService);

        return challengeGameDto;
    }

}
