package org.polemon.viki.controller.message.command;

public enum CommandEnum {
    HELP("help - Display a list of all available commands. viki help <command>\n"),
    PING("ping - I will print pong (I love to play ping pong :D).\n"),

    REGISTER("register - Register yourself as a new user.\n"),
    CHALLENGE("challenge <player> <[game] | random> - Challenge player to a specific or random game\n"),

    SETTINGS("settings - Display a list of settings to change. (Admin only)\n"),

    TEST(""),
    INVALID("");

    private Command command;
    private final String helpMessage;


    CommandEnum(String helpMessage) {
        this.helpMessage = helpMessage;
    }


    public void setCommand(Command command) {
        this.command = command;
    }


    public Command getCommand() {
        return command;
    }


    public String getHelpMessage() {
        return helpMessage;
    }

}
