package org.polemon.viki.controller.message.command;

import org.javacord.api.event.message.MessageCreateEvent;
import org.polemon.viki.Messages;
import org.springframework.stereotype.Component;

@Component
public class InvalidCommand implements Command {

    @Override
    public String execute(MessageCreateEvent event) {
        return Messages.INVALID_COMMAND;
    }

}
