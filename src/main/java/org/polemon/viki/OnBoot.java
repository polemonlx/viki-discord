package org.polemon.viki;

import org.polemon.viki.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OnBoot {

    private UserService userService;


    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    public void init() {
        restoreAccounts();
    }


    private void restoreAccounts() {
        userService.restoreAccounts();
    }

}
