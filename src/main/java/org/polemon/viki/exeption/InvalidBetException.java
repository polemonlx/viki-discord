package org.polemon.viki.exeption;

import org.polemon.viki.Messages;

public class InvalidBetException extends VikiException {

    @Override
    public String toString() {
        return Messages.ERROR_INVALID_BET;
    }

}
