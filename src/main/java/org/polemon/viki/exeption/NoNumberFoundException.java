package org.polemon.viki.exeption;

import org.polemon.viki.Messages;

public class NoNumberFoundException extends VikiException {

    @Override
    public String toString() {
        return Messages.ERROR_NO_NUMBER_FOUND;
    }

}
