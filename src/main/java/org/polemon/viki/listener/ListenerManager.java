package org.polemon.viki.listener;

import org.javacord.api.entity.user.User;
import org.polemon.viki.listener.game.PlayingMultiplayer;
import org.polemon.viki.listener.game.PlayingSolo;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ListenerManager {

    private final ConcurrentHashMap<User, List<ActivityListener>> activeUsersList = new ConcurrentHashMap<>();


    public int countListeners(User user) {

        if (!activeUsersList.containsKey(user)) {
            return 0;
        }
        return activeUsersList.get(user).size();
    }


    public ActivityListener getListener(User user, String[] arguments) {

        List<ActivityListener> listeners = activeUsersList.get(user);

        if (arguments.length == 2) {
            return listeners.get(0);
        }

        for (ActivityListener listener : listeners) {
            if (listener.getName().equalsIgnoreCase(arguments[1])) {
                return listener;
            }
        }

        return null;
    }


    public void addListener(User user, ActivityListener listener) {

        List<ActivityListener> listeners = new LinkedList<>();

        if (activeUsersList.containsKey(user)) {
            listeners = activeUsersList.get(user);
        }

        listeners.add(listener);

        activeUsersList.put(user, listeners);

    }


    public void removeListener(User user, ActivityListener listener) {
        activeUsersList.get(user).remove(listener);
    }


    public boolean isPlayingMultiplayer(User user) {

        if (!activeUsersList.containsKey(user)) {
            return false;
        }

        List<ActivityListener> listeners = activeUsersList.get(user);

        for (ActivityListener listener : listeners) {
            if (listener instanceof PlayingMultiplayer) {
                return true;
            }
        }

        return false;

    }


    public boolean isPlayingSolo(User user) {

        if (!activeUsersList.containsKey(user)) {
            return false;
        }

        List<ActivityListener> listeners = activeUsersList.get(user);

        for (ActivityListener listener : listeners) {
            if (listener instanceof PlayingSolo) {
                return true;
            }
        }

        return false;
    }

}
