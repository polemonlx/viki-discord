package org.polemon.viki.listener.game;

import org.javacord.api.entity.user.User;
import org.polemon.viki.Messages;
import org.polemon.viki.dto.ChallengeGameDto;
import org.polemon.viki.game.util.Classification;
import org.polemon.viki.service.UserService;

import java.util.*;

public class GameListenerMultiplayer implements PlayingMultiplayer, StartGame, EndGame {

    private final ChallengeGameDto challengeGameDto;

    private final List<User> playersEliminated = new LinkedList<>();

    private final List<User> playersQuited = new LinkedList<>();


    public GameListenerMultiplayer(ChallengeGameDto challengeGameDto) {
        this.challengeGameDto = challengeGameDto;
    }


    @Override
    public String execute(User player, String[] arguments) {

        String[] gameArguments = Arrays.copyOfRange(arguments, 1, arguments.length);
        if (arguments[1].equalsIgnoreCase(getName())) {
            gameArguments = Arrays.copyOfRange(arguments, 2, arguments.length);
        }

        if (gameArguments[0].equalsIgnoreCase("quit") && gameArguments[1].equalsIgnoreCase("game")) {
            return removeUser(player);
        }

        return challengeGameDto.getGameController().execute(player, gameArguments);
    }


    @Override
    public String getName() {
        return challengeGameDto.getName();
    }


    @Override
    public String init() {
        return challengeGameDto.getGameController().init(challengeGameDto.getPlayers(), this, challengeGameDto.getOptions());
    }


    @Override
    public void finish(List<User> playersOrder) {

        Classification classification = prepareClassification(playersOrder);
        UserService userService = challengeGameDto.getUserService();

        userService.gameFinished(classification);
    }


    @Override
    public void userEliminated(User player) {
        playersEliminated.add(player);
        challengeGameDto.getListenerManager().removeListener(player, this);
    }


    private Classification prepareClassification(List<User> playersOrder) {

        int numberRewards = (int) Math.max(5, Math.ceil(playersOrder.size() / 2D));
        int partsToDivide = calculateAmountOfParts(numberRewards);
        int rewardPerPart = challengeGameDto.getBet() / partsToDivide;

        Map<Long, Integer> playerRewardMap = new LinkedHashMap<>();
        for (int i = 0; i < playersOrder.size(); i++) {
            playerRewardMap.put(playersOrder.get(i).getId(), Math.max(0, rewardPerPart * (numberRewards - i)));
        }

        Classification classification = new Classification();
        classification.setPlayerRewardMap(playerRewardMap);
        classification.setBet(challengeGameDto.getBet());

        return classification;
    }


    private int calculateAmountOfParts(int numberRewards) {
        int parts = 0;

        for (int i = 1; i <= numberRewards; i++) {
            parts += i;
        }

        return parts;
    }


    private String removeUser(User player) {

        String response = Messages.SUCCESS_REMOVED;

        playersQuited.add(player);
        challengeGameDto.getGameController().userQuit(player);
        challengeGameDto.getListenerManager().removeListener(player, this);

        if (challengeGameDto.getPlayers().size() - playersQuited.size() < challengeGameDto.getMinPlayers()) {
            cancelGame();
            response = Messages.ERROR_NOT_ENOUGH_PLAYERS + " The game has been canceled.";
        }

        return response;
    }


    private void cancelGame() {

        removeAllListeners();

        UserService userService = challengeGameDto.getUserService();
        Classification classification = prepareCancelGame();
        if (classification == null) {
            return;
        }
        userService.gameFinished(classification);
    }


    private Classification prepareCancelGame() {

        int numberRewards = challengeGameDto.getPlayers().size() - playersQuited.size() - playersEliminated.size();
        if (numberRewards == 0) {
            return null;
        }

        int rewardPerPlayer = (challengeGameDto.getBet() * challengeGameDto.getPlayers().size()) / numberRewards;

        Classification classification = new Classification();
        classification.setBet(challengeGameDto.getBet());

        for (User player : challengeGameDto.getPlayers()) {
            int reward = rewardPerPlayer;
            if (playersEliminated.contains(player) || playersQuited.contains(player)) {
                reward = 0;
            }
            classification.addPlayerReward(player, reward);
        }

        return classification;
    }


    private void removeAllListeners() {
        for (User player : challengeGameDto.getPlayers()) {
            challengeGameDto.getListenerManager().removeListener(player, this);
        }
    }

}
