package org.polemon.viki.listener.game;

import org.javacord.api.entity.DiscordEntity;
import org.javacord.api.entity.user.User;
import org.polemon.viki.Messages;
import org.polemon.viki.dto.ChallengeGameDto;
import org.polemon.viki.exeption.InvalidBetException;
import org.polemon.viki.exeption.NoNumberFoundException;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Bet implements PlayingMultiplayer {

    private final ChallengeGameDto challengeGameDto;

    private final Map<User, Boolean> playersBet = new LinkedHashMap<>();


    public Bet(ChallengeGameDto challengeGameDto) {
        this.challengeGameDto = challengeGameDto;
        for (User player : challengeGameDto.getPlayers()) {
            playersBet.put(player, false);
        }
    }


    @Override
    public String execute(User user, String[] arguments) {

        String argumentString = getArgumentString(arguments);

        int argumentInt;
        try {
            argumentInt = getArgumentInt(argumentString);
        } catch (NoNumberFoundException | InvalidBetException e) {
            return e.toString();
        }

        for (User player : challengeGameDto.getPlayers()) {

            if (user.equals(player)) {

                if (argumentString.equalsIgnoreCase("accept")) {

                    if (challengeGameDto.getUserService().get(user.getId()).getMoney() < challengeGameDto.getBet()) {
                        return Messages.ERROR_NOT_ENOUGH_MONEY;
                    }

                    playersBet.put(player, true);

                    if (hasAllAccepted()) {
                        try {
                            User playerError = debitAccounts();
                            if (playerError != null) {
                                return playerError.getMentionTag() + " doesn't have enought money for the bet.";
                            }
                        } catch (Exception ex) {
                            return "A user has not enough money to complete the bet.";
                        }
                        return changeListeners();
                    }

                } else if (argumentString.equalsIgnoreCase("quit")) {
                    quitGame(player);
                    if (!hasPlayersEnough()) {
                        stopGame();
                        return Messages.ERROR_NOT_ENOUGH_PLAYERS;
                    }

                } else {

                    if (challengeGameDto.getUserService().get(user.getId()).getMoney() < argumentInt) {
                        return Messages.ERROR_NOT_ENOUGH_MONEY;
                    }

                    challengeGameDto.setBet(argumentInt);
                    resetAcceptedBets();
                }
                return "Actual bet: " + challengeGameDto.getBet() + ". Players accepted bet/ Players left: " + getPlayersAcceptCount() + " / " + playersBet.size();
            }
        }

        return null; // Not a possible player
    }


    @Override
    public String getName() {
        return challengeGameDto.getName();
    }


    private User debitAccounts() throws Exception {
        List<Long> ids = challengeGameDto.getPlayers().stream().map(DiscordEntity::getId).collect(Collectors.toList());
        long userErrorId = challengeGameDto.getUserService().transferBetToCaptive(ids, challengeGameDto.getBet());
        if (userErrorId != 0) {
            return challengeGameDto.getPlayers().stream().filter(x -> x.getId() == userErrorId).findFirst().get();
        }

        return null;
    }


    private int getArgumentInt(String argumentString) throws NoNumberFoundException, InvalidBetException {
        int argumentInt = 0;
        try {
            if (!argumentString.equalsIgnoreCase("accept") && !argumentString.equalsIgnoreCase("quit"))
                argumentInt = Integer.parseInt(argumentString);
        } catch (NumberFormatException ex) {
            throw new NoNumberFoundException();
        }

        if (argumentInt < 0) {
            throw new InvalidBetException();
        }
        return argumentInt;
    }


    private void stopGame() {
        for (User user : challengeGameDto.getPlayers()) {
            quitGame(user);
        }
    }


    private boolean hasPlayersEnough() {
        return playersBet.size() >= challengeGameDto.getMinPlayers();
    }


    private String getPlayersAcceptCount() {
        int sum = 0;
        for (Boolean validation : playersBet.values()) {
            if (validation) {
                sum += 1;
            }
        }
        return Integer.toString(sum);
    }


    private void quitGame(User player) {
        playersBet.remove(player);
        challengeGameDto.getPlayers().remove(player);
        challengeGameDto.getListenerManager().removeListener(player, this);
    }


    private void resetAcceptedBets() {
        for (User player : challengeGameDto.getPlayers()) {
            playersBet.put(player, false);
        }
    }


    private String changeListeners() {
        GameListenerMultiplayer gameListenerMultiplayer = new GameListenerMultiplayer(challengeGameDto);
        String response = gameListenerMultiplayer.init();
        for (User player : challengeGameDto.getPlayers()) {
            challengeGameDto.getListenerManager().removeListener(player, this);
            challengeGameDto.getListenerManager().addListener(player, gameListenerMultiplayer);
        }
        return response;
    }


    private boolean hasAllAccepted() {
        for (Boolean validation : playersBet.values()) {
            if (!validation) {
                return false;
            }
        }
        return true;
    }


    private String getArgumentString(String[] arguments) {
        if (arguments.length == 2) {
            return arguments[1];
        }
        return arguments[2];
    }

}
