package org.polemon.viki.listener.game;

import org.javacord.api.entity.user.User;
import org.polemon.viki.game.util.Classification;

import java.util.List;

public interface EndGame {

    void finish(List<User> playersOrder);

    void userEliminated(User player);

}
