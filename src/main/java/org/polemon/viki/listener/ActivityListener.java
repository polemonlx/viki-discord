package org.polemon.viki.listener;

import org.javacord.api.entity.user.User;
import org.polemon.viki.game.util.Classification;

public interface ActivityListener {

    String execute(User user, String[] arguments);

    String getName();

}
