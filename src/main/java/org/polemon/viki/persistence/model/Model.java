package org.polemon.viki.persistence.model;

public interface Model {

    long getId();

    void setId(long id);

}
