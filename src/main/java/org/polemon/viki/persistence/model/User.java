package org.polemon.viki.persistence.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.awt.*;

@Entity
@Table(name = "user")
public class User extends AbstractModel {

    private String name;
    private Integer money;
    private Integer points;

    private Integer captive_money;

    private Long totalTime;
    private Long lastSeenTime;
    private Long lastRewardTime;


    public User() {

    }


    public User(long id, String name) {
        super.setId(id);
        this.name = name;
        this.money = 0;
        this.points = 0;

        this.captive_money = 0;

        this.lastSeenTime = System.currentTimeMillis();
        this.totalTime = 0L;
        this.lastRewardTime = 0L;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public Integer getMoney() {
        return money;
    }


    public void setMoney(Integer money) {
        this.money = money;
    }


    public Integer getPoints() {
        return points;
    }


    public void setPoints(Integer points) {
        this.points = points;
    }


    public Integer getCaptive_money() {
        return captive_money;
    }


    public void setCaptive_money(Integer captive_money) {
        this.captive_money = captive_money;
    }


    public Long getTotalTime() {
        return totalTime;
    }


    public void setTotalTime(Long total_time_seconds) {
        this.totalTime = total_time_seconds;
    }


    public Long getLastSeenTime() {
        return lastSeenTime;
    }


    public void setLastSeenTime(Long last_seen) {
        this.lastSeenTime = last_seen;
    }


    public Long getLastRewardTime() {
        return lastRewardTime;
    }


    public void setLastRewardTime(Long last_reward_seconds) {
        this.lastRewardTime = last_reward_seconds;
    }

}
