package org.polemon.viki.persistence.dao.jpa;

import org.polemon.viki.persistence.dao.UserDao;
import org.polemon.viki.persistence.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class JpaUserDao extends JpaAbstractDao<User> implements UserDao {

    public JpaUserDao() {
        super(User.class);
    }


    @Override
    public List<User> getOnline() {

        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);

        Root<User> root = criteriaQuery.from(User.class);

        criteriaQuery.select(root);

        criteriaQuery.where(builder.notEqual(root.get("lastSeenTime"), -1));

        return em.createQuery(criteriaQuery).getResultList();
    }

}
