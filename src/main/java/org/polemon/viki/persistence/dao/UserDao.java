package org.polemon.viki.persistence.dao;

import org.polemon.viki.persistence.model.User;

import java.util.List;

public interface UserDao extends Dao<User>{

    List<User> getOnline();

}
