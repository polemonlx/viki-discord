package org.polemon.viki.persistence.dao;

import java.util.List;

public interface Dao<T> {

    // CRUD

    List<T> findAll();


    T findById(long id);


    T saveOrUpdate(T t);


    void delete(long id);

}
