package org.polemon.viki.persistence.dao.jpa;

import org.polemon.viki.persistence.dao.Dao;
import org.polemon.viki.persistence.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public abstract class JpaAbstractDao<T extends AbstractModel> implements Dao<T> {

    protected Class<T> modelType;

    @PersistenceContext
    protected EntityManager em;


    public JpaAbstractDao(Class<T> modelType) {
        this.modelType = modelType;
    }


    public void setEm(EntityManager em) {
        this.em = em;
    }


    @Override
    public List<T> findAll() {
        return em.createQuery("from " + modelType.getSimpleName(), modelType).getResultList();
    }


    @Override
    public T findById(long id) {
        return em.find(modelType, id);
    }


    @Override
    public T saveOrUpdate(T t) {
        return em.merge(t);
    }


    @Override
    public void delete(long id) {
        em.remove(findById(id));
    }

}
