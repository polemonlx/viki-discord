package org.polemon.viki.service;

import org.javacord.api.DiscordApi;
import org.polemon.viki.Config;
import org.polemon.viki.game.util.Classification;
import org.polemon.viki.persistence.dao.UserDao;
import org.polemon.viki.persistence.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class UserServiceImpl implements UserService, CrudService<User> {

    private DiscordApi api;

    private UserDao userDao;


    @Autowired
    public void setApi(DiscordApi api) {
        this.api = api;
    }


    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }


    @Override
    public List<User> list() {
        return userDao.findAll();
    }


    @Override
    public User get(long id) {
        return userDao.findById(id);
    }


    @Transactional
    @Override
    public User save(User modelObject) {
        return userDao.saveOrUpdate(modelObject);
    }


    @Transactional
    @Override
    public void delete(long id) {
        userDao.delete(id);
    }


    @Override
    public List<User> onlineUsers() {
        return userDao.getOnline();
    }


    @Transactional
    @Override
    public void activityReward(long id) {

        User user = userDao.findById(id);

        if (user.getLastSeenTime() == -1L) {
            user.setLastSeenTime(System.currentTimeMillis());
        }

        int money = user.getMoney();
        long totalTime = user.getTotalTime();
        long lastSeenTime = user.getLastSeenTime();
        long lastRewardTime = user.getLastRewardTime();

        long onlineTime = System.currentTimeMillis() - lastSeenTime;

        if (lastRewardTime + onlineTime >= Config.TIME_FOR_REWARD_MILLISECONDS) {
            money += Config.REWARD_AMOUNT_PER_INTERVAL;
            lastRewardTime += onlineTime - Config.TIME_FOR_REWARD_MILLISECONDS;
        }

        totalTime += onlineTime;
        lastSeenTime = -1L;

        try {
            if (api.getUserById(user.getId()).get().getConnectedVoiceChannels().size() != 0) {
                lastSeenTime = System.currentTimeMillis();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        user.setMoney(money);
        user.setTotalTime(totalTime);
        user.setLastSeenTime(lastSeenTime);
        user.setLastRewardTime(lastRewardTime);

        userDao.saveOrUpdate(user);
    }


    @Transactional
    @Override
    public void gameFinished(Classification classification) {
        for (Long id : classification.getPlayerRewardMap().keySet()) {
            User user = userDao.findById(id);
            user.setCaptive_money(user.getCaptive_money() - classification.getBet());
            user.setMoney(user.getMoney() + classification.getPlayerRewardMap().get(id));
            userDao.saveOrUpdate(user);
        }
    }


    @Transactional
    @Override
    public long transferBetToCaptive(List<Long> ids, Integer bet) throws Exception {

        for (Long id : ids) {
            User user = userDao.findById(id);
            if (user.getMoney() < bet) {
                return id;
            }
            user.setMoney(user.getMoney() - bet);
            user.setCaptive_money(user.getCaptive_money() + bet);
            userDao.saveOrUpdate(user);
        }

        return 0;
    }


    @Override
    public void restoreAccounts() {
        for (User user : userDao.findAll()) {
            if (user.getCaptive_money() > 0) {
                user.setMoney(user.getMoney() + user.getCaptive_money());
                user.setCaptive_money(0);
                userDao.saveOrUpdate(user);
            }
        }
    }

}
