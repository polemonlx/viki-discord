package org.polemon.viki.service;

import org.polemon.viki.persistence.model.AbstractModel;

import java.util.List;

public interface CrudService<T extends AbstractModel> {

    // CRUD

    List<T> list();

    T get(long id);

    T save(T modelObject);

    void delete(long id);

}
