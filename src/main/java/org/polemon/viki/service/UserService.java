package org.polemon.viki.service;

import org.hibernate.internal.ExceptionMapperStandardImpl;
import org.polemon.viki.game.util.Classification;
import org.polemon.viki.persistence.model.User;

import java.util.List;

public interface UserService extends CrudService<User> {

    List<User> onlineUsers();

    void activityReward(long id);

    void gameFinished(Classification classification);

    long transferBetToCaptive(List<Long> ids, Integer bet) throws Exception;

    void restoreAccounts();

}
