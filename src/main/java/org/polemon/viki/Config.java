package org.polemon.viki;

import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.intent.Intent;
import org.javacord.api.entity.server.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    public static final String BOT_TOKEN = "NzYwMjk5MDk4MTMyODQwNDU5.X3KBtg.bRkih7Cr19JJDEktVUc3fK-tWJE";
    public static final String ACTIVE_PROFILE = "prod";
    public static final String RESOURCES_LOCATION = "/spring/spring-config.xml";

    public static final int UPDATE_STATUS_INTERVAL_MILLISECONDS = 600000;
    public static final int TIME_FOR_REWARD_MILLISECONDS = 3600000;
    public static final int REWARD_AMOUNT_PER_INTERVAL = 50;


    @Bean
    public DiscordApi createApi() {

        DiscordApi api = new DiscordApiBuilder()
                .setToken(BOT_TOKEN)
                .setAllIntents()
                .login()
                .join();


        // Create invite link
        System.out.println("Bot invite link: " + api.createBotInvite());

        return api;
    }

}
