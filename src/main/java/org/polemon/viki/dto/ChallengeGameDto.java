package org.polemon.viki.dto;

import org.javacord.api.entity.user.User;
import org.polemon.viki.game.controller.GameController;
import org.polemon.viki.listener.ListenerManager;
import org.polemon.viki.service.UserService;

import java.util.List;

public class ChallengeGameDto {

    private String name;

    private List<User> players;

    private int minPlayers;

    private int maxPlayers;

    private String[] options;

    private Integer bet = 0;

    private GameController gameController;

    private ListenerManager listenerManager;

    private UserService userService;


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public List<User> getPlayers() {
        return players;
    }


    public void setPlayers(List<User> players) {
        this.players = players;
    }


    public int getMinPlayers() {
        return minPlayers;
    }


    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
    }


    public int getMaxPlayers() {
        return maxPlayers;
    }


    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }


    public String[] getOptions() {
        return options;
    }


    public void setOptions(String[] options) {
        this.options = options;
    }


    public Integer getBet() {
        return bet;
    }


    public void setBet(Integer bet) {
        this.bet = bet;
    }


    public GameController getGameController() {
        return gameController;
    }


    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }


    public ListenerManager getListenerManager() {
        return listenerManager;
    }


    public void setListenerManager(ListenerManager listenerManager) {
        this.listenerManager = listenerManager;
    }


    public UserService getUserService() {
        return userService;
    }


    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
